# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.4.0] - 2024-09-24

### Added

- **Breaking** The library now include a low resolution dithering, instead of 5x5 pixels the low res dither 15x15 pixels at a time. Just add `true` in the call to dither to keep the current behavior.

## [0.3.2] - 2024-09-18

### Added

- Tests

### Changed

- Update image to version 0.25.2

### Fixed

- For images with alpha layer the color is now correctly calculated

## [0.3.1] - 2023-04-26

### Fixed

- Process grayscale image without crashing

## [0.3.0] - 2023-04-13

### Added

- Overwrite flag

## [0.2.1] - 2023-04-02

### Changed

- Refactor and reduce slightly the execution time

## [0.2.0] - 2023-03-07

### Added

- Support for png with transparency

### Fixed

- Version in changelog

## [0.1.0] - 2022-11-18

### Added

- First version of this library
