use image::RgbaImage;
mod color_finder;
mod coordinates;
mod square;
use square::Square;
mod setting;
use setting::Setting;

pub fn dithor(
    in_path: &str,
    out_path: &str,
    force_ouput_overwrite: bool,
    color: bool,
    high_res: bool,
) {
    files_presence(in_path, out_path, force_ouput_overwrite);
    let settings = Setting::new(in_path, color, high_res);
    let mut new_img: RgbaImage = image::ImageBuffer::new(settings.width, settings.height);
    for j in 0..settings.height_factor {
        for i in 0..settings.width_factor {
            let square = create_square(i, j, &settings);
            new_img = square.colorize(new_img);
        }
    }
    new_img.save(out_path).unwrap();
}

fn create_square(i: u32, j: u32, settings: &Setting) -> Square {
    let a = i * settings.square_width * settings.coef;
    let b = j * settings.width * settings.square_width * settings.coef;
    let k = a + b;
    let mut groupped_pixels: Vec<u8> = Vec::new();
    for n in 0..(settings.square_width) {
        let mut group = create_groupped_pixels(n, k, settings).to_vec();
        groupped_pixels.append(&mut group);
    }
    let colors_info = settings.get_colors(&groupped_pixels[0..settings.end_range]);
    Square::new(
        i * settings.square_width,
        j * settings.square_width,
        settings.square_width,
        colors_info,
    )
}

fn create_groupped_pixels(counter: u32, k: u32, settings: &Setting) -> &[u8] {
    &settings.bytes[((k + (counter * settings.c)) as usize)
        ..((k + (counter * settings.c) + settings.square_width * settings.coef) as usize)]
}

fn files_presence(in_img: &str, out_img: &str, force_ouput_overwrite: bool) {
    let in_file = std::path::Path::new(in_img).exists();
    let out_file = std::path::Path::new(out_img).exists();
    if in_file == false {
        panic!("The input file provided doesn't exist")
    }
    if out_file == true && force_ouput_overwrite == false {
        panic!("The output file provided alredy exists")
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::fs;
    use std::path::Path;

    #[test]
    fn dithor_black_and_white_alpha() {
        dithor(
            "test/gradiant_alpha.png",
            "test/dithor_baw_alpha.png",
            true,
            false,
            true,
        );
        assert!(Path::new("test/dithor_baw_alpha.png").exists());
        fs::remove_file("test/dithor_baw_alpha.png").unwrap();
    }

    #[test]
    fn dithor_black_and_white() {
        dithor(
            "test/gradiant.png",
            "test/dithor_baw.png",
            true,
            false,
            true,
        );
        assert!(Path::new("test/dithor_baw.png").exists());
        fs::remove_file("test/dithor_baw.png").unwrap();
    }

    #[test]
    fn dithor_black_and_white_large() {
        dithor(
            "test/test.png",
            "test/dithor_baw_alpha_large.png",
            true,
            false,
            false,
        );
        assert!(Path::new("test/dithor_baw_alpha_large.png").exists());
        fs::remove_file("test/dithor_baw_alpha_large.png").unwrap();
    }

    #[test]
    fn dithor_color_alpha() {
        dithor(
            "test/gradiant_alpha.png",
            "test/dithor_color_alpha.png",
            true,
            true,
            true,
        );
        assert!(Path::new("test/dithor_color_alpha.png").exists());
        fs::remove_file("test/dithor_color_alpha.png").unwrap();
    }

    #[test]
    fn dithor_color() {
        dithor(
            "test/gradiant.png",
            "test/dithor_color.png",
            true,
            true,
            true,
        );
        assert!(Path::new("test/dithor_color.png").exists());
        fs::remove_file("test/dithor_color.png").unwrap();
    }

    #[test]
    fn dithor_color_large() {
        dithor(
            "test/test.png",
            "test/dithor_color_large.png",
            true,
            true,
            false,
        );
        assert!(Path::new("test/dithor_color_large.png").exists());
        fs::remove_file("test/dithor_color_large.png").unwrap();
    }
}
