use crate::color_finder::*;
use image;

pub struct Setting {
    color_finder: Box<dyn ColorFinder>,
    pub c: u32,
    pub coef: u32,
    pub end_range: usize,
    pub width_factor: u32,
    pub height_factor: u32,
    pub width: u32,
    pub height: u32,
    pub bytes: Vec<u8>,
    pub high_res: bool,
    pub square_width: u32,
}

impl Setting {
    pub fn new(in_path: &str, dithor_in_color: bool, high_res: bool) -> Self {
        let in_img = image::open(in_path).unwrap();
        let has_alpha = in_img.color().has_alpha();
        let has_color = in_img.color().has_color();
        let coef = Self::coef(has_alpha, has_color);
        let width = in_img.width();
        let c = coef * width;
        let square_width = Self::define_square_pixel_size(high_res);
        Self {
            color_finder: Self::define_color_finder(dithor_in_color, has_alpha, has_color),
            c,
            coef,
            end_range: ((square_width * square_width) * coef) as usize,
            width_factor: in_img.width() / square_width,
            height_factor: in_img.height() / square_width,
            width,
            height: in_img.height(),
            bytes: in_img.into_bytes(),
            high_res,
            square_width,
        }
    }

    fn define_square_pixel_size(high_res: bool) -> u32 {
        if high_res {
            5
        } else {
            15
        }
    }

    fn coef(has_alpha: bool, has_color: bool) -> u32 {
        if has_alpha && has_color {
            4
        } else if has_alpha {
            2
        } else if has_color {
            3
        } else {
            1
        }
    }

    fn define_color_finder(
        dithor_in_color: bool,
        has_alpha: bool,
        has_color: bool,
    ) -> Box<dyn ColorFinder> {
        if dithor_in_color && has_alpha && has_color {
            Box::new(ImageColorAlpha {})
        } else if dithor_in_color && has_color {
            Box::new(ImageColor {})
        } else if has_alpha && has_color {
            Box::new(ImageAlpha {})
        } else if has_color {
            Box::new(ImageBW {})
        } else if has_alpha {
            Box::new(ImageGrayscaleAlpha {})
        } else {
            Box::new(ImageGrayscale {})
        }
    }

    pub fn get_colors(&self, image: &[u8]) -> ColorInfo {
        self.color_finder.get_colors(image, self.high_res)
    }
}
