pub struct ImageBW {}
pub struct ImageAlpha {}
pub struct ImageColor {}
pub struct ImageColorAlpha {}
pub struct ImageGrayscale {}
pub struct ImageGrayscaleAlpha {}
use image::Rgba;

pub struct ColorInfo {
    pub level: u32,
    pub base_color: Rgba<u8>,
    pub filling_color: Rgba<u8>,
}

impl ColorInfo {
    pub fn new(level: u32, light: [u8; 4], dark: [u8; 4], high_res: bool) -> Self {
        let colors = Self::define_base_color(level, light, dark, high_res);
        Self {
            level,
            base_color: image::Rgba(colors.0),
            filling_color: image::Rgba(colors.1),
        }
    }

    fn define_base_color(
        level: u32,
        light: [u8; 4],
        dark: [u8; 4],
        high_res: bool,
    ) -> ([u8; 4], [u8; 4]) {
        if (level < 10 && !high_res) || level < 6 {
            (dark, light)
        } else {
            (light, dark)
        }
    }
}

fn get_level(luminance: f32, size: usize) -> u32 {
    let average_luminance = luminance / (size as f32);
    let divider = if size == 25 { 25.50 } else { 12.14 };
    let float_level = average_luminance / divider;
    float_level.trunc() as u32
}

pub trait ColorFinder {
    fn get_colors(&self, image: &[u8], high_res: bool) -> ColorInfo;
}

impl ColorFinder for ImageBW {
    fn get_colors(&self, image: &[u8], high_res: bool) -> ColorInfo {
        let mut sum = 0.00;

        for x in 0..image.len() / 3 {
            let red = f32::from(image[x * 3]);
            let green = f32::from(image[x * 3 + 1]);
            let blue = f32::from(image[x * 3 + 2]);
            let luminance = 0.2126 * red + 0.7152 * green + 0.0722 * blue;
            sum = sum + luminance;
        }

        ColorInfo::new(
            get_level(sum, image.len() / 3),
            [255, 255, 255, 255],
            [0, 0, 0, 255],
            high_res,
        )
    }
}

impl ColorFinder for ImageAlpha {
    fn get_colors(&self, image: &[u8], high_res: bool) -> ColorInfo {
        let mut sum = 0.00;
        let mut alpha = 0;

        for x in 0..image.len() / 4 {
            let d = image[x * 4 + 3];
            if d > alpha {
                alpha = d;
            }

            let red = f32::from(image[x * 4]);
            let green = f32::from(image[x * 4 + 1]);
            let blue = f32::from(image[x * 4 + 2]);
            let luminance = 0.2126 * red + 0.7152 * green + 0.0722 * blue;
            sum = sum + luminance;
        }

        ColorInfo::new(
            get_level(sum, image.len() / 4),
            [255, 255, 255, alpha],
            [0, 0, 0, alpha],
            high_res,
        )
    }
}

impl ColorFinder for ImageColorAlpha {
    fn get_colors(&self, image: &[u8], high_res: bool) -> ColorInfo {
        let mut sum = 0.00;
        let mut min_red = 255;
        let mut min_green = 255;
        let mut min_blue = 255;
        let mut min_alpha = 255;
        let mut max_red = 0;
        let mut max_green = 0;
        let mut max_blue = 0;
        let mut max_alpha = 255;
        for x in 0..image.len() / 4 {
            let a = image[x * 4];
            let b = image[x * 4 + 1];
            let c = image[x * 4 + 2];
            if min_red > a && min_green > b && min_blue > c {
                min_red = a;
                min_green = b;
                min_blue = c;
                min_alpha = image[x * 4 + 3];
            }
            if max_red < a && max_green < b && max_blue < c {
                max_red = a;
                max_green = b;
                max_blue = c;
                max_alpha = image[x * 4 + 3];
            }
            let red = f32::from(image[x * 4]);
            let green = f32::from(image[x * 4 + 1]);
            let blue = f32::from(image[x * 4 + 2]);
            let luminance = 0.2126 * red + 0.7152 * green + 0.0722 * blue;
            sum = sum + luminance;
        }

        ColorInfo::new(
            get_level(sum, image.len() / 4),
            [max_red, max_green, max_blue, max_alpha],
            [min_red, min_green, min_blue, min_alpha],
            high_res,
        )
    }
}

impl ColorFinder for ImageColor {
    fn get_colors(&self, image: &[u8], high_res: bool) -> ColorInfo {
        let mut sum = 0.00;
        let mut min_red = 255;
        let mut min_green = 255;
        let mut min_blue = 255;
        let mut max_red = 0;
        let mut max_green = 0;
        let mut max_blue = 0;
        for x in 0..image.len() / 4 {
            let a = image[x * 3];
            let b = image[x * 3 + 1];
            let c = image[x * 3 + 2];
            if min_red > a && min_green > b && min_blue > c {
                min_red = a;
                min_green = b;
                min_blue = c;
            }
            if max_red < a && max_green < b && max_blue < c {
                max_red = a;
                max_green = b;
                max_blue = c;
            }
            let red = f32::from(image[x * 3]);
            let green = f32::from(image[x * 3 + 1]);
            let blue = f32::from(image[x * 3 + 2]);
            let luminance = 0.2126 * red + 0.7152 * green + 0.0722 * blue;
            sum = sum + luminance;
        }

        ColorInfo::new(
            get_level(sum, image.len() / 4),
            [max_red, max_green, max_blue, 255],
            [min_red, min_green, min_blue, 255],
            high_res,
        )
    }
}

impl ColorFinder for ImageGrayscale {
    fn get_colors(&self, image: &[u8], high_res: bool) -> ColorInfo {
        let mut sum = 0.00;

        for x in 0..image.len() / 3 {
            let luminance = f32::from(image[x]);
            sum = sum + luminance;
        }

        ColorInfo::new(
            get_level(sum, image.len() / 3),
            [255, 255, 255, 255],
            [0, 0, 0, 255],
            high_res,
        )
    }
}

impl ColorFinder for ImageGrayscaleAlpha {
    fn get_colors(&self, image: &[u8], high_res: bool) -> ColorInfo {
        let mut sum = 0.00;
        let mut alpha = 0;

        for x in 0..image.len() / 4 {
            let d = image[x * 3 + 1];
            if d > alpha {
                alpha = d;
            }
            let luminance = f32::from(image[x * 3]);
            sum = sum + luminance;
        }

        ColorInfo::new(
            get_level(sum, image.len() / 4),
            [255, 255, 255, alpha],
            [0, 0, 0, alpha],
            high_res,
        )
    }
}
