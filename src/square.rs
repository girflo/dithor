use crate::color_finder::ColorInfo;
use crate::coordinates::Coordinates;
use image::Rgba;
use image::RgbaImage;

pub struct Square {
    xoffset: u32,
    yoffset: u32,
    size: u32,
    filling_coordinates: Coordinates,
    base_color: Rgba<u8>,
    filling_color: Rgba<u8>,
}

impl Square {
    pub fn new(xoffset: u32, yoffset: u32, size: u32, colors_info: ColorInfo) -> Self {
        let filling_coordinates = Coordinates::new(xoffset, yoffset, colors_info.level, size);
        Self {
            xoffset,
            yoffset,
            size,
            filling_coordinates,
            base_color: colors_info.base_color,
            filling_color: colors_info.filling_color,
        }
    }

    pub fn colorize(&self, image: RgbaImage) -> RgbaImage {
        let base_image = self.write_base(image);
        let colorized_image = self.write_filling(base_image);
        colorized_image
    }

    fn write_base(&self, mut image: RgbaImage) -> RgbaImage {
        for x in 0..self.size {
            for y in 0..self.size {
                image.put_pixel(self.xoffset + x, self.yoffset + y, self.base_color);
            }
        }
        image
    }

    fn write_filling(&self, mut image: RgbaImage) -> RgbaImage {
        for coordinates in self.filling_coordinates.coordinates.iter() {
            image.put_pixel(coordinates.0, coordinates.1, self.filling_color);
        }
        image
    }
}
