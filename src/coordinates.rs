pub struct Coordinates {
    pub coordinates: Vec<(u32, u32)>,
}

impl Coordinates {
    pub fn new(xoffset: u32, yoffset: u32, color_level: u32, size: u32) -> Self {
        let coordinates = if size < 6 {
            Self::small_square_coordinates(xoffset, yoffset, color_level)
        } else {
            Self::big_square_coordinates(xoffset, yoffset, color_level)
        };
        Self { coordinates }
    }

    fn big_square_coordinates(xoffset: u32, yoffset: u32, color_level: u32) -> Vec<(u32, u32)> {
        match color_level {
            1 => Self::big_level_one_coordinates(xoffset, yoffset),
            2 => Self::big_level_two_coordinates(xoffset, yoffset),
            3 => Self::big_level_three_coordinates(xoffset, yoffset),
            4 => Self::big_level_four_coordinates(xoffset, yoffset),
            5 => Self::big_level_five_coordinates(xoffset, yoffset),
            6 => Self::big_level_six_coordinates(xoffset, yoffset),
            7 => Self::big_level_seven_coordinates(xoffset, yoffset),
            8 => Self::big_level_height_coordinates(xoffset, yoffset),
            9 => Self::big_level_nine_coordinates(xoffset, yoffset),
            10 => Self::big_level_nine_coordinates(xoffset, yoffset),
            11 => Self::big_level_height_coordinates(xoffset, yoffset),
            12 => Self::big_level_seven_coordinates(xoffset, yoffset),
            13 => Self::big_level_six_coordinates(xoffset, yoffset),
            14 => Self::big_level_five_coordinates(xoffset, yoffset),
            15 => Self::big_level_four_coordinates(xoffset, yoffset),
            16 => Self::big_level_three_coordinates(xoffset, yoffset),
            17 => Self::big_level_two_coordinates(xoffset, yoffset),
            18 => Self::big_level_one_coordinates(xoffset, yoffset),
            _ => vec![],
        }
    }

    fn big_level_one_coordinates(xoffset: u32, yoffset: u32) -> Vec<(u32, u32)> {
        vec![
            (xoffset + 3, yoffset + 3),
            (xoffset + 11, yoffset + 3),
            (xoffset + 3, yoffset + 11),
            (xoffset + 11, yoffset + 11),
        ]
    }

    fn big_level_two_coordinates(xoffset: u32, yoffset: u32) -> Vec<(u32, u32)> {
        vec![
            (xoffset + 3, yoffset + 3),
            (xoffset + 7, yoffset + 3),
            (xoffset + 11, yoffset + 3),
            (xoffset + 1, yoffset + 5),
            (xoffset + 13, yoffset + 5),
            (xoffset + 3, yoffset + 7),
            (xoffset + 7, yoffset + 7),
            (xoffset + 11, yoffset + 7),
            (xoffset + 1, yoffset + 9),
            (xoffset + 13, yoffset + 9),
            (xoffset + 3, yoffset + 11),
            (xoffset + 7, yoffset + 11),
            (xoffset + 11, yoffset + 11),
        ]
    }

    fn big_level_three_coordinates(xoffset: u32, yoffset: u32) -> Vec<(u32, u32)> {
        vec![
            (xoffset + 1, yoffset + 1),
            (xoffset + 5, yoffset + 1),
            (xoffset + 9, yoffset + 1),
            (xoffset + 13, yoffset + 1),
            (xoffset + 3, yoffset + 3),
            (xoffset + 7, yoffset + 3),
            (xoffset + 11, yoffset + 3),
            (xoffset + 1, yoffset + 5),
            (xoffset + 5, yoffset + 5),
            (xoffset + 9, yoffset + 5),
            (xoffset + 13, yoffset + 5),
            (xoffset + 3, yoffset + 7),
            (xoffset + 7, yoffset + 7),
            (xoffset + 11, yoffset + 7),
            (xoffset + 1, yoffset + 9),
            (xoffset + 5, yoffset + 9),
            (xoffset + 9, yoffset + 9),
            (xoffset + 13, yoffset + 9),
            (xoffset + 3, yoffset + 11),
            (xoffset + 7, yoffset + 11),
            (xoffset + 11, yoffset + 11),
            (xoffset + 1, yoffset + 13),
            (xoffset + 5, yoffset + 13),
            (xoffset + 9, yoffset + 13),
            (xoffset + 13, yoffset + 13),
        ]
    }

    fn big_level_four_coordinates(xoffset: u32, yoffset: u32) -> Vec<(u32, u32)> {
        vec![
            (xoffset + 1, yoffset + 1),
            (xoffset + 5, yoffset + 1),
            (xoffset + 7, yoffset + 1),
            (xoffset + 9, yoffset + 1),
            (xoffset + 13, yoffset + 1),
            (xoffset + 1, yoffset + 3),
            (xoffset + 3, yoffset + 3),
            (xoffset + 5, yoffset + 3),
            (xoffset + 9, yoffset + 3),
            (xoffset + 11, yoffset + 3),
            (xoffset + 13, yoffset + 3),
            (xoffset + 1, yoffset + 5),
            (xoffset + 5, yoffset + 5),
            (xoffset + 7, yoffset + 5),
            (xoffset + 9, yoffset + 5),
            (xoffset + 13, yoffset + 5),
            (xoffset + 1, yoffset + 7),
            (xoffset + 3, yoffset + 7),
            (xoffset + 5, yoffset + 7),
            (xoffset + 9, yoffset + 7),
            (xoffset + 11, yoffset + 7),
            (xoffset + 13, yoffset + 7),
            (xoffset + 1, yoffset + 9),
            (xoffset + 5, yoffset + 9),
            (xoffset + 7, yoffset + 9),
            (xoffset + 9, yoffset + 9),
            (xoffset + 13, yoffset + 9),
            (xoffset + 1, yoffset + 11),
            (xoffset + 3, yoffset + 11),
            (xoffset + 5, yoffset + 11),
            (xoffset + 9, yoffset + 11),
            (xoffset + 11, yoffset + 11),
            (xoffset + 13, yoffset + 11),
            (xoffset + 1, yoffset + 13),
            (xoffset + 5, yoffset + 13),
            (xoffset + 7, yoffset + 13),
            (xoffset + 9, yoffset + 13),
            (xoffset + 13, yoffset + 13),
        ]
    }

    fn big_level_five_coordinates(xoffset: u32, yoffset: u32) -> Vec<(u32, u32)> {
        vec![
            (xoffset + 1, yoffset + 1),
            (xoffset + 3, yoffset + 1),
            (xoffset + 5, yoffset + 1),
            (xoffset + 7, yoffset + 1),
            (xoffset + 9, yoffset + 1),
            (xoffset + 11, yoffset + 1),
            (xoffset + 13, yoffset + 1),
            (xoffset + 1, yoffset + 3),
            (xoffset + 3, yoffset + 3),
            (xoffset + 5, yoffset + 3),
            (xoffset + 7, yoffset + 3),
            (xoffset + 9, yoffset + 3),
            (xoffset + 11, yoffset + 3),
            (xoffset + 13, yoffset + 3),
            (xoffset + 1, yoffset + 5),
            (xoffset + 3, yoffset + 5),
            (xoffset + 5, yoffset + 5),
            (xoffset + 7, yoffset + 5),
            (xoffset + 9, yoffset + 5),
            (xoffset + 11, yoffset + 5),
            (xoffset + 13, yoffset + 5),
            (xoffset + 1, yoffset + 7),
            (xoffset + 3, yoffset + 7),
            (xoffset + 5, yoffset + 7),
            (xoffset + 7, yoffset + 7),
            (xoffset + 9, yoffset + 7),
            (xoffset + 11, yoffset + 7),
            (xoffset + 13, yoffset + 7),
            (xoffset + 1, yoffset + 9),
            (xoffset + 3, yoffset + 9),
            (xoffset + 5, yoffset + 9),
            (xoffset + 7, yoffset + 9),
            (xoffset + 9, yoffset + 9),
            (xoffset + 11, yoffset + 9),
            (xoffset + 13, yoffset + 9),
            (xoffset + 1, yoffset + 11),
            (xoffset + 3, yoffset + 11),
            (xoffset + 5, yoffset + 11),
            (xoffset + 7, yoffset + 11),
            (xoffset + 9, yoffset + 11),
            (xoffset + 11, yoffset + 11),
            (xoffset + 13, yoffset + 11),
            (xoffset + 1, yoffset + 13),
            (xoffset + 3, yoffset + 13),
            (xoffset + 5, yoffset + 13),
            (xoffset + 7, yoffset + 13),
            (xoffset + 9, yoffset + 13),
            (xoffset + 11, yoffset + 13),
            (xoffset + 13, yoffset + 13),
        ]
    }

    fn big_level_six_coordinates(xoffset: u32, yoffset: u32) -> Vec<(u32, u32)> {
        vec![
            (xoffset + 1, yoffset),
            (xoffset + 5, yoffset),
            (xoffset + 9, yoffset),
            (xoffset + 13, yoffset),
            (xoffset, yoffset + 1),
            (xoffset + 2, yoffset + 1),
            (xoffset + 4, yoffset + 1),
            (xoffset + 6, yoffset + 1),
            (xoffset + 8, yoffset + 1),
            (xoffset + 10, yoffset + 1),
            (xoffset + 12, yoffset + 1),
            (xoffset + 14, yoffset + 1),
            (xoffset + 2, yoffset + 3),
            (xoffset + 4, yoffset + 3),
            (xoffset + 6, yoffset + 3),
            (xoffset + 8, yoffset + 3),
            (xoffset + 10, yoffset + 3),
            (xoffset + 12, yoffset + 3),
            (xoffset + 14, yoffset + 3),
            (xoffset + 1, yoffset + 4),
            (xoffset + 5, yoffset + 4),
            (xoffset + 9, yoffset + 4),
            (xoffset + 13, yoffset + 4),
            (xoffset, yoffset + 5),
            (xoffset + 2, yoffset + 5),
            (xoffset + 4, yoffset + 5),
            (xoffset + 6, yoffset + 5),
            (xoffset + 8, yoffset + 5),
            (xoffset + 10, yoffset + 5),
            (xoffset + 12, yoffset + 5),
            (xoffset + 14, yoffset + 5),
            (xoffset, yoffset + 7),
            (xoffset + 2, yoffset + 7),
            (xoffset + 4, yoffset + 7),
            (xoffset + 6, yoffset + 7),
            (xoffset + 8, yoffset + 7),
            (xoffset + 10, yoffset + 7),
            (xoffset + 12, yoffset + 7),
            (xoffset + 14, yoffset + 7),
            (xoffset + 1, yoffset + 8),
            (xoffset + 5, yoffset + 8),
            (xoffset + 9, yoffset + 8),
            (xoffset + 13, yoffset + 8),
            (xoffset, yoffset + 9),
            (xoffset + 2, yoffset + 9),
            (xoffset + 4, yoffset + 9),
            (xoffset + 6, yoffset + 9),
            (xoffset + 8, yoffset + 9),
            (xoffset + 10, yoffset + 9),
            (xoffset + 12, yoffset + 9),
            (xoffset + 14, yoffset + 9),
            (xoffset, yoffset + 11),
            (xoffset + 2, yoffset + 11),
            (xoffset + 4, yoffset + 11),
            (xoffset + 6, yoffset + 11),
            (xoffset + 8, yoffset + 11),
            (xoffset + 10, yoffset + 11),
            (xoffset + 12, yoffset + 11),
            (xoffset + 14, yoffset + 11),
            (xoffset + 1, yoffset + 12),
            (xoffset + 5, yoffset + 12),
            (xoffset + 9, yoffset + 12),
            (xoffset + 13, yoffset + 12),
            (xoffset, yoffset + 13),
            (xoffset + 2, yoffset + 13),
            (xoffset + 4, yoffset + 13),
            (xoffset + 6, yoffset + 13),
            (xoffset + 8, yoffset + 13),
            (xoffset + 10, yoffset + 13),
            (xoffset + 12, yoffset + 13),
            (xoffset + 14, yoffset + 13),
        ]
    }

    fn big_level_seven_coordinates(xoffset: u32, yoffset: u32) -> Vec<(u32, u32)> {
        vec![
            (xoffset + 1, yoffset),
            (xoffset + 5, yoffset),
            (xoffset + 9, yoffset),
            (xoffset + 13, yoffset),
            (xoffset, yoffset + 1),
            (xoffset + 2, yoffset + 1),
            (xoffset + 4, yoffset + 1),
            (xoffset + 6, yoffset + 1),
            (xoffset + 8, yoffset + 1),
            (xoffset + 10, yoffset + 1),
            (xoffset + 12, yoffset + 1),
            (xoffset + 14, yoffset + 1),
            (xoffset + 3, yoffset + 2),
            (xoffset + 7, yoffset + 2),
            (xoffset + 11, yoffset + 2),
            (xoffset + 2, yoffset + 3),
            (xoffset + 4, yoffset + 3),
            (xoffset + 6, yoffset + 3),
            (xoffset + 8, yoffset + 3),
            (xoffset + 10, yoffset + 3),
            (xoffset + 12, yoffset + 3),
            (xoffset + 14, yoffset + 3),
            (xoffset + 1, yoffset + 4),
            (xoffset + 5, yoffset + 4),
            (xoffset + 9, yoffset + 4),
            (xoffset + 13, yoffset + 4),
            (xoffset, yoffset + 5),
            (xoffset + 2, yoffset + 5),
            (xoffset + 4, yoffset + 5),
            (xoffset + 6, yoffset + 5),
            (xoffset + 8, yoffset + 5),
            (xoffset + 10, yoffset + 5),
            (xoffset + 12, yoffset + 5),
            (xoffset + 14, yoffset + 5),
            (xoffset + 3, yoffset + 6),
            (xoffset + 7, yoffset + 6),
            (xoffset + 11, yoffset + 6),
            (xoffset, yoffset + 7),
            (xoffset + 2, yoffset + 7),
            (xoffset + 4, yoffset + 7),
            (xoffset + 6, yoffset + 7),
            (xoffset + 8, yoffset + 7),
            (xoffset + 10, yoffset + 7),
            (xoffset + 12, yoffset + 7),
            (xoffset + 14, yoffset + 7),
            (xoffset + 1, yoffset + 8),
            (xoffset + 5, yoffset + 8),
            (xoffset + 9, yoffset + 8),
            (xoffset + 13, yoffset + 8),
            (xoffset, yoffset + 9),
            (xoffset + 2, yoffset + 9),
            (xoffset + 4, yoffset + 9),
            (xoffset + 6, yoffset + 9),
            (xoffset + 8, yoffset + 9),
            (xoffset + 10, yoffset + 9),
            (xoffset + 12, yoffset + 9),
            (xoffset + 14, yoffset + 9),
            (xoffset + 3, yoffset + 10),
            (xoffset + 7, yoffset + 10),
            (xoffset + 11, yoffset + 10),
            (xoffset, yoffset + 11),
            (xoffset + 2, yoffset + 11),
            (xoffset + 4, yoffset + 11),
            (xoffset + 6, yoffset + 11),
            (xoffset + 8, yoffset + 11),
            (xoffset + 10, yoffset + 11),
            (xoffset + 12, yoffset + 11),
            (xoffset + 14, yoffset + 11),
            (xoffset + 1, yoffset + 12),
            (xoffset + 5, yoffset + 12),
            (xoffset + 9, yoffset + 12),
            (xoffset + 13, yoffset + 12),
            (xoffset, yoffset + 13),
            (xoffset + 2, yoffset + 13),
            (xoffset + 4, yoffset + 13),
            (xoffset + 6, yoffset + 13),
            (xoffset + 8, yoffset + 13),
            (xoffset + 10, yoffset + 13),
            (xoffset + 12, yoffset + 13),
            (xoffset + 14, yoffset + 13),
            (xoffset + 3, yoffset + 14),
            (xoffset + 7, yoffset + 14),
            (xoffset + 11, yoffset + 14),
        ]
    }

    fn big_level_height_coordinates(xoffset: u32, yoffset: u32) -> Vec<(u32, u32)> {
        vec![
            (xoffset + 1, yoffset),
            (xoffset + 5, yoffset),
            (xoffset + 9, yoffset),
            (xoffset + 13, yoffset),
            (xoffset, yoffset + 1),
            (xoffset + 2, yoffset + 1),
            (xoffset + 4, yoffset + 1),
            (xoffset + 6, yoffset + 1),
            (xoffset + 8, yoffset + 1),
            (xoffset + 10, yoffset + 1),
            (xoffset + 12, yoffset + 1),
            (xoffset + 14, yoffset + 1),
            (xoffset + 1, yoffset + 2),
            (xoffset + 3, yoffset + 2),
            (xoffset + 5, yoffset + 2),
            (xoffset + 7, yoffset + 2),
            (xoffset + 9, yoffset + 2),
            (xoffset + 11, yoffset + 2),
            (xoffset + 13, yoffset + 2),
            (xoffset + 2, yoffset + 3),
            (xoffset + 6, yoffset + 3),
            (xoffset + 8, yoffset + 3),
            (xoffset + 12, yoffset + 3),
            (xoffset + 1, yoffset + 4),
            (xoffset + 3, yoffset + 4),
            (xoffset + 5, yoffset + 4),
            (xoffset + 7, yoffset + 4),
            (xoffset + 9, yoffset + 4),
            (xoffset + 11, yoffset + 4),
            (xoffset + 13, yoffset + 4),
            (xoffset, yoffset + 5),
            (xoffset + 2, yoffset + 5),
            (xoffset + 4, yoffset + 5),
            (xoffset + 6, yoffset + 5),
            (xoffset + 8, yoffset + 5),
            (xoffset + 10, yoffset + 5),
            (xoffset + 12, yoffset + 5),
            (xoffset + 14, yoffset + 5),
            (xoffset + 1, yoffset + 6),
            (xoffset + 3, yoffset + 6),
            (xoffset + 5, yoffset + 6),
            (xoffset + 7, yoffset + 6),
            (xoffset + 9, yoffset + 6),
            (xoffset + 11, yoffset + 6),
            (xoffset + 13, yoffset + 6),
            (xoffset + 2, yoffset + 7),
            (xoffset + 6, yoffset + 7),
            (xoffset + 8, yoffset + 7),
            (xoffset + 12, yoffset + 7),
            (xoffset + 1, yoffset + 8),
            (xoffset + 3, yoffset + 8),
            (xoffset + 5, yoffset + 8),
            (xoffset + 7, yoffset + 8),
            (xoffset + 9, yoffset + 8),
            (xoffset + 11, yoffset + 8),
            (xoffset + 13, yoffset + 8),
            (xoffset, yoffset + 9),
            (xoffset + 2, yoffset + 9),
            (xoffset + 4, yoffset + 9),
            (xoffset + 6, yoffset + 9),
            (xoffset + 8, yoffset + 9),
            (xoffset + 10, yoffset + 9),
            (xoffset + 12, yoffset + 9),
            (xoffset + 14, yoffset + 9),
            (xoffset + 1, yoffset + 10),
            (xoffset + 3, yoffset + 10),
            (xoffset + 5, yoffset + 10),
            (xoffset + 7, yoffset + 10),
            (xoffset + 9, yoffset + 10),
            (xoffset + 11, yoffset + 10),
            (xoffset + 13, yoffset + 10),
            (xoffset + 2, yoffset + 11),
            (xoffset + 6, yoffset + 11),
            (xoffset + 8, yoffset + 11),
            (xoffset + 12, yoffset + 11),
            (xoffset + 1, yoffset + 12),
            (xoffset + 3, yoffset + 12),
            (xoffset + 5, yoffset + 12),
            (xoffset + 7, yoffset + 12),
            (xoffset + 9, yoffset + 12),
            (xoffset + 11, yoffset + 12),
            (xoffset + 13, yoffset + 12),
            (xoffset, yoffset + 13),
            (xoffset + 2, yoffset + 13),
            (xoffset + 4, yoffset + 13),
            (xoffset + 6, yoffset + 13),
            (xoffset + 8, yoffset + 13),
            (xoffset + 10, yoffset + 13),
            (xoffset + 12, yoffset + 13),
            (xoffset + 14, yoffset + 13),
            (xoffset + 1, yoffset + 14),
            (xoffset + 5, yoffset + 14),
            (xoffset + 9, yoffset + 14),
            (xoffset + 13, yoffset + 14),
        ]
    }

    fn big_level_nine_coordinates(xoffset: u32, yoffset: u32) -> Vec<(u32, u32)> {
        vec![
            (xoffset + 1, yoffset),
            (xoffset + 3, yoffset),
            (xoffset + 5, yoffset),
            (xoffset + 7, yoffset),
            (xoffset + 9, yoffset),
            (xoffset + 11, yoffset),
            (xoffset + 13, yoffset),
            (xoffset, yoffset + 1),
            (xoffset + 2, yoffset + 1),
            (xoffset + 4, yoffset + 1),
            (xoffset + 6, yoffset + 1),
            (xoffset + 8, yoffset + 1),
            (xoffset + 10, yoffset + 1),
            (xoffset + 12, yoffset + 1),
            (xoffset + 14, yoffset + 1),
            (xoffset + 1, yoffset + 2),
            (xoffset + 3, yoffset + 2),
            (xoffset + 5, yoffset + 2),
            (xoffset + 7, yoffset + 2),
            (xoffset + 9, yoffset + 2),
            (xoffset + 11, yoffset + 2),
            (xoffset + 13, yoffset + 2),
            (xoffset, yoffset + 3),
            (xoffset + 2, yoffset + 3),
            (xoffset + 6, yoffset + 3),
            (xoffset + 8, yoffset + 3),
            (xoffset + 12, yoffset + 3),
            (xoffset + 14, yoffset + 3),
            (xoffset + 1, yoffset + 4),
            (xoffset + 3, yoffset + 4),
            (xoffset + 5, yoffset + 4),
            (xoffset + 9, yoffset + 4),
            (xoffset + 11, yoffset + 4),
            (xoffset + 13, yoffset + 4),
            (xoffset, yoffset + 5),
            (xoffset + 2, yoffset + 5),
            (xoffset + 4, yoffset + 5),
            (xoffset + 6, yoffset + 5),
            (xoffset + 8, yoffset + 5),
            (xoffset + 10, yoffset + 5),
            (xoffset + 12, yoffset + 5),
            (xoffset + 14, yoffset + 5),
            (xoffset + 1, yoffset + 6),
            (xoffset + 3, yoffset + 6),
            (xoffset + 5, yoffset + 6),
            (xoffset + 7, yoffset + 6),
            (xoffset + 9, yoffset + 6),
            (xoffset + 11, yoffset + 6),
            (xoffset + 13, yoffset + 6),
            (xoffset, yoffset + 7),
            (xoffset + 2, yoffset + 7),
            (xoffset + 4, yoffset + 7),
            (xoffset + 6, yoffset + 7),
            (xoffset + 8, yoffset + 7),
            (xoffset + 10, yoffset + 7),
            (xoffset + 12, yoffset + 7),
            (xoffset + 14, yoffset + 7),
            (xoffset + 1, yoffset + 8),
            (xoffset + 3, yoffset + 8),
            (xoffset + 5, yoffset + 8),
            (xoffset + 7, yoffset + 8),
            (xoffset + 9, yoffset + 8),
            (xoffset + 11, yoffset + 8),
            (xoffset + 13, yoffset + 8),
            (xoffset, yoffset + 9),
            (xoffset + 2, yoffset + 9),
            (xoffset + 4, yoffset + 9),
            (xoffset + 6, yoffset + 9),
            (xoffset + 8, yoffset + 9),
            (xoffset + 10, yoffset + 9),
            (xoffset + 12, yoffset + 9),
            (xoffset + 14, yoffset + 9),
            (xoffset + 1, yoffset + 10),
            (xoffset + 3, yoffset + 10),
            (xoffset + 5, yoffset + 10),
            (xoffset + 9, yoffset + 10),
            (xoffset + 11, yoffset + 10),
            (xoffset + 13, yoffset + 10),
            (xoffset, yoffset + 11),
            (xoffset + 2, yoffset + 11),
            (xoffset + 6, yoffset + 11),
            (xoffset + 8, yoffset + 11),
            (xoffset + 12, yoffset + 11),
            (xoffset + 14, yoffset + 11),
            (xoffset + 1, yoffset + 12),
            (xoffset + 3, yoffset + 12),
            (xoffset + 5, yoffset + 12),
            (xoffset + 7, yoffset + 12),
            (xoffset + 9, yoffset + 12),
            (xoffset + 11, yoffset + 12),
            (xoffset + 13, yoffset + 12),
            (xoffset, yoffset + 13),
            (xoffset + 2, yoffset + 13),
            (xoffset + 4, yoffset + 13),
            (xoffset + 6, yoffset + 13),
            (xoffset + 8, yoffset + 13),
            (xoffset + 10, yoffset + 13),
            (xoffset + 12, yoffset + 13),
            (xoffset + 14, yoffset + 13),
            (xoffset + 1, yoffset + 14),
            (xoffset + 3, yoffset + 14),
            (xoffset + 5, yoffset + 14),
            (xoffset + 7, yoffset + 14),
            (xoffset + 9, yoffset + 14),
            (xoffset + 11, yoffset + 14),
            (xoffset + 13, yoffset + 14),
        ]
    }

    fn small_square_coordinates(xoffset: u32, yoffset: u32, color_level: u32) -> Vec<(u32, u32)> {
        match color_level {
            1 => Self::level_one_coordinates(xoffset, yoffset),
            2 => Self::level_two_coordinates(xoffset, yoffset),
            3 => Self::level_three_coordinates(xoffset, yoffset),
            4 => Self::level_four_coordinates(xoffset, yoffset),
            5 => Self::level_five_coordinates(xoffset, yoffset),
            6 => Self::level_six_coordinates(xoffset, yoffset),
            7 => Self::level_seven_coordinates(xoffset, yoffset),
            8 => Self::level_height_coordinates(xoffset, yoffset),
            _ => vec![],
        }
    }

    fn level_height_coordinates(xoffset: u32, yoffset: u32) -> Vec<(u32, u32)> {
        vec![(xoffset + 2, yoffset + 2)]
    }

    fn level_seven_coordinates(xoffset: u32, yoffset: u32) -> Vec<(u32, u32)> {
        vec![
            (xoffset, yoffset + 2),
            (xoffset + 2, yoffset),
            (xoffset + 2, yoffset + 4),
            (xoffset + 4, yoffset + 2),
        ]
    }

    fn level_six_coordinates(xoffset: u32, yoffset: u32) -> Vec<(u32, u32)> {
        vec![
            (xoffset, yoffset),
            (xoffset, yoffset + 2),
            (xoffset, yoffset + 4),
            (xoffset + 2, yoffset),
            (xoffset + 2, yoffset + 2),
            (xoffset + 2, yoffset + 4),
            (xoffset + 4, yoffset),
            (xoffset + 4, yoffset + 2),
            (xoffset + 4, yoffset + 4),
        ]
    }

    fn level_five_coordinates(xoffset: u32, yoffset: u32) -> Vec<(u32, u32)> {
        vec![
            (xoffset, yoffset),
            (xoffset, yoffset + 2),
            (xoffset, yoffset + 4),
            (xoffset + 1, yoffset + 1),
            (xoffset + 2, yoffset),
            (xoffset + 2, yoffset + 2),
            (xoffset + 2, yoffset + 4),
            (xoffset + 3, yoffset + 3),
            (xoffset + 4, yoffset),
            (xoffset + 4, yoffset + 2),
            (xoffset + 4, yoffset + 4),
        ]
    }

    fn level_four_coordinates(xoffset: u32, yoffset: u32) -> Vec<(u32, u32)> {
        vec![
            (xoffset, yoffset + 1),
            (xoffset, yoffset + 3),
            (xoffset + 1, yoffset),
            (xoffset + 1, yoffset + 2),
            (xoffset + 1, yoffset + 4),
            (xoffset + 2, yoffset + 1),
            (xoffset + 2, yoffset + 3),
            (xoffset + 3, yoffset),
            (xoffset + 3, yoffset + 2),
            (xoffset + 3, yoffset + 4),
            (xoffset + 4, yoffset + 1),
            (xoffset + 4, yoffset + 3),
        ]
    }

    fn level_three_coordinates(xoffset: u32, yoffset: u32) -> Vec<(u32, u32)> {
        vec![
            (xoffset, yoffset),
            (xoffset, yoffset + 4),
            (xoffset + 1, yoffset + 1),
            (xoffset + 1, yoffset + 3),
            (xoffset + 2, yoffset + 2),
            (xoffset + 3, yoffset + 1),
            (xoffset + 3, yoffset + 3),
            (xoffset + 4, yoffset),
            (xoffset + 4, yoffset + 4),
        ]
    }

    fn level_two_coordinates(xoffset: u32, yoffset: u32) -> Vec<(u32, u32)> {
        vec![
            (xoffset, yoffset + 1),
            (xoffset + 2, yoffset + 1),
            (xoffset + 4, yoffset + 1),
            (xoffset, yoffset + 3),
            (xoffset + 2, yoffset + 3),
            (xoffset + 4, yoffset + 3),
        ]
    }

    fn level_one_coordinates(xoffset: u32, yoffset: u32) -> Vec<(u32, u32)> {
        vec![
            (xoffset, yoffset + 2),
            (xoffset + 2, yoffset),
            (xoffset + 2, yoffset + 4),
            (xoffset + 4, yoffset + 2),
        ]
    }
}
